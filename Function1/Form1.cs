﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Function1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x, a1, a2, b, l, m, c, y;
            x = Convert.ToDouble(textBox1.Text);
            a1 = Convert.ToDouble(textBox2.Text);
            a2 = Convert.ToDouble(textBox3.Text);
            b = Convert.ToDouble(textBox4.Text);
            l = Convert.ToDouble(textBox5.Text);
            c = 1 / Math.Pow(x, 2) + Math.Pow(a1, 2);
            if (Math.Pow(x, 2) + Math.Pow(a1, 2) == 0)
            {
                textBox6.Text = ("Решения нет");
            }
            else
            {
                m = Math.Sqrt(Math.Pow(x, 3) * (b - a2) - Math.Pow(Math.Cos(l + 2), 3));
                if (m < 0)
                {
                    textBox6.Text = ("Решения нет");
                }
                else
                {
                    y = c * m;
                    textBox6.Text += y;
                }


            }
        }
    }
}
